
#include "Vulkan.hpp"

#include <limits>
#include <sstream>

#include <Flat/Core/Utils.hpp>

#include "LogTypes.hpp"




namespace Flat { namespace Vulkan {




void checkResult(const VkResult res)
{
	if (FLAT_LIKELY(res == VK_SUCCESS)) return;

	if (res < 0) {
		switch (res) {
		case VK_ERROR_OUT_OF_DATE_KHR:
			return;
		default:;
		}
	}

	const char * const name = resultName(res);

	std::stringstream s;
	s << "VkResult=" << (name ? name : "unknown") << "(" << int(res) << ")";
	throw Error(s.str());
}




namespace Tools {




void submitAndWait(const VkCommandBuffer commandBuffer, const VkQueue queue)
{
	VkSubmitInfo submitInfo = {};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitInfo.waitSemaphoreCount = 0;
	submitInfo.pWaitSemaphores = nullptr;
	submitInfo.pWaitDstStageMask = nullptr;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &commandBuffer;
	submitInfo.signalSemaphoreCount = 0;
	submitInfo.pSignalSemaphores = nullptr;
	checkResult(vkQueueSubmit(queue, 1, &submitInfo, nullptr));
	checkResult(vkQueueWaitIdle(queue));
};


uint32_t getMemoryTypeIndex(const VkPhysicalDeviceMemoryProperties & gpuMemoryProperties,
		uint32_t typeBits, const VkMemoryPropertyFlags p, VkBool32 * const found)
{
	for (uint32_t i = 0; i < gpuMemoryProperties.memoryTypeCount; i++) {
		if ((typeBits & 1) == 1) {
			if ((gpuMemoryProperties.memoryTypes[i].propertyFlags & p) == p) {
				if (found) *found = true;
				return i;
			}
		}
		typeBits >>= 1;
	}

	if (found) {
		*found = false;
		return 0;
	}

	throw Error("Could not find a matching memory type");
};




} // Tools




}} // Flat::Vulkan
