
#pragma once

#include <array>
#include <vector>
#include <stdexcept>
#include <string>

#include <vulkan/vulkan.h>




namespace Flat { namespace Vulkan {




class Error : public std::runtime_error
{
public:
	Error(const std::string & what = std::string()) : std::runtime_error(what) {}
};

extern FLAT_VULKAN_EXPORT void checkResult(VkResult res);




template <typename T>
struct __HandleTraits;

template <typename T>
struct __HandleAllocTraits
{
	static T null() noexcept { return nullptr; }
	static bool isNull(const T & value) noexcept { return value == null(); }
};

template <typename T>
class Handle : public __HandleTraits<T>::Deps
{
public:
	typedef __HandleTraits<T> Traits;
	typedef typename __HandleTraits<T>::Deps Deps;

	Handle(const Deps & deps, T value) noexcept :
		Deps(deps),
		value_(value)
	{}

	Handle() noexcept {}

	template <typename I>
	Handle(const Deps & deps, const I & info) :
		Deps(deps)
	{
		const VkResult res = Traits::create(deps, info, value_, nullptr);
		checkResult(res);
	}

	~Handle() noexcept
	{
		if (!__HandleAllocTraits<T>::isNull(value_)) {
			Traits::destroy(static_cast<const Deps&>(*this), value_, nullptr);
		}
	}

	Handle(Handle && o) noexcept :
		Deps(static_cast<const Deps&>(o)),
		value_(o.value_)
	{
		o.value_ = __HandleAllocTraits<T>::null();
	}

	Handle & operator=(Handle && o) noexcept
	{
		std::swap(static_cast<Deps&>(*this), static_cast<Deps&>(o));
		std::swap(value_, o.value_);
		return *this;
	}

	Handle(const Handle&) = delete;
	void operator=(const Handle&) = delete;

	operator bool() const noexcept { return value_ != nullptr; }
	operator const T &() const noexcept { return value_; }
	const T * get() const noexcept { return &value_; }

private:
	T value_ = __HandleAllocTraits<T>::null();
};




template <>
struct __HandleTraits<VkInstance>
{
	struct Deps {};

	static VkResult create(const Deps &, const VkInstanceCreateInfo & info, VkInstance & value,
			const VkAllocationCallbacks * const a) noexcept
	{
		return vkCreateInstance(&info, a, &value);
	}

	static void destroy(const Deps &, const VkInstance & value,
			const VkAllocationCallbacks * const a) noexcept
	{
		vkDestroyInstance(value, a);
	}
};

typedef Handle<VkInstance> Instance;




template <>
struct __HandleTraits<VkDevice>
{
	struct Deps {
		VkPhysicalDevice gpu;
	};

	static VkResult create(const Deps & deps, const VkDeviceCreateInfo & info,
			VkDevice & value, const VkAllocationCallbacks * const a) noexcept
	{
		return vkCreateDevice(deps.gpu, &info, a, &value);
	}

	static void destroy(const Deps &, const VkDevice & value,
			const VkAllocationCallbacks * const a) noexcept
	{
		vkDestroyDevice(value, a);
	}
};

typedef Handle<VkDevice> Device;




template <>
struct __HandleTraits<VkSemaphore>
{
	struct Deps {
		VkDevice device;
	};

	static VkResult create(const Deps & deps, const VkSemaphoreCreateInfo & info,
			VkSemaphore & value, const VkAllocationCallbacks * const a) noexcept
	{
		return vkCreateSemaphore(deps.device, &info, a, &value);
	}

	static void destroy(const Deps & deps, const VkSemaphore & value,
			const VkAllocationCallbacks * const a) noexcept
	{
		vkDestroySemaphore(deps.device, value, a);
	}
};

typedef Handle<VkSemaphore> Semaphore;




template <>
struct __HandleTraits<VkFence>
{
	struct Deps {
		VkDevice device;
	};

	static VkResult create(const Deps & deps, const VkFenceCreateInfo & info,
			VkFence & value, const VkAllocationCallbacks * const a) noexcept
	{
		return vkCreateFence(deps.device, &info, a, &value);
	}

	static void destroy(const Deps & deps, const VkFence & value,
			const VkAllocationCallbacks * const a) noexcept
	{
		vkDestroyFence(deps.device, value, a);
	}
};

typedef Handle<VkFence> Fence;




template <>
struct __HandleTraits<VkCommandPool>
{
	struct Deps {
		VkDevice device;
	};

	static VkResult create(const Deps & deps, const VkCommandPoolCreateInfo & info,
			VkCommandPool & value, const VkAllocationCallbacks * const a) noexcept
	{
		return vkCreateCommandPool(deps.device, &info, a, &value);
	}

	static void destroy(const Deps & deps, const VkCommandPool & value,
			const VkAllocationCallbacks * const a) noexcept
	{
		vkDestroyCommandPool(deps.device, value, a);
	}
};

typedef Handle<VkCommandPool> CommandPool;




template <>
struct __HandleTraits<VkCommandBuffer>
{
	struct Deps {
		VkDevice device;
		VkCommandPool commandPool;
	};

	static VkResult create(const Deps & deps, const VkCommandBufferAllocateInfo & info,
			VkCommandBuffer & value, const VkAllocationCallbacks *) noexcept
	{
		return vkAllocateCommandBuffers(deps.device, &info, &value);
	}

	static void destroy(const Deps & deps, const VkCommandBuffer & value,
			const VkAllocationCallbacks *) noexcept
	{
		vkFreeCommandBuffers(deps.device, deps.commandPool, 1, &value);
	}
};

typedef Handle<VkCommandBuffer> CommandBuffer;




template <int Size>
using __CommandBufferArray = std::array<VkCommandBuffer, Size>;

template <int Size>
struct __HandleTraits<__CommandBufferArray<Size>>
{
	struct Deps {
		VkDevice device;
		VkCommandPool commandPool;
		int size;
	};

	static VkResult create(const Deps & deps, const VkCommandBufferAllocateInfo & info,
			__CommandBufferArray<Size> & value, const VkAllocationCallbacks *) noexcept
	{
		return vkAllocateCommandBuffers(deps.device, &info, value.data());
	}

	static void destroy(const Deps & deps, const __CommandBufferArray<Size> & value,
			const VkAllocationCallbacks *) noexcept
	{
		vkFreeCommandBuffers(deps.device, deps.commandPool, deps.size, value.data());
	}
};

template <int Size>
using CommandBufferArray = Handle<__CommandBufferArray<Size>>;




template <>
struct __HandleAllocTraits<std::vector<VkCommandBuffer>>
{
	static std::vector<VkCommandBuffer> null() noexcept { return {}; };
	static bool isNull(const std::vector<VkCommandBuffer> & value) noexcept { return value.empty(); }
};

template <>
struct __HandleTraits<std::vector<VkCommandBuffer>>
{
	struct Deps {
		VkDevice device;
		VkCommandPool commandPool;
	};

	static VkResult create(const Deps & deps, const VkCommandBufferAllocateInfo & info,
			std::vector<VkCommandBuffer> & value, const VkAllocationCallbacks *) noexcept
	{
		value.resize(info.commandBufferCount);
		return vkAllocateCommandBuffers(deps.device, &info, value.data());
	}

	static void destroy(const Deps & deps, const std::vector<VkCommandBuffer> & value,
			const VkAllocationCallbacks *) noexcept
	{
		vkFreeCommandBuffers(deps.device, deps.commandPool, uint32_t(value.size()), value.data());
	}
};

typedef Handle<std::vector<VkCommandBuffer>> CommandBufferVector;




template <>
struct __HandleTraits<VkImage>
{
	struct Deps {
		VkDevice device;
	};

	static VkResult create(const Deps & deps, const VkImageCreateInfo & info, VkImage & value,
			const VkAllocationCallbacks * const a) noexcept
	{
		return vkCreateImage(deps.device, &info, a, &value);
	}

	static void destroy(const Deps & deps, const VkImage & value,
			const VkAllocationCallbacks * const a) noexcept
	{
		vkDestroyImage(deps.device, value, a);
	}
};

typedef Handle<VkImage> Image;




template <>
struct __HandleTraits<VkDeviceMemory>
{
	struct Deps {
		VkDevice device;
	};

	static VkResult create(const Deps & deps, const VkMemoryAllocateInfo & info,
			VkDeviceMemory & value, const VkAllocationCallbacks * const a) noexcept
	{
		return vkAllocateMemory(deps.device, &info, a, &value);
	}

	static void destroy(const Deps & deps, const VkDeviceMemory & value,
			const VkAllocationCallbacks * const a) noexcept
	{
		vkFreeMemory(deps.device, value, a);
	}
};

typedef Handle<VkDeviceMemory> DeviceMemory;




template <>
struct __HandleTraits<VkImageView>
{
	struct Deps {
		VkDevice device;
	};

	static VkResult create(const Deps & deps, const VkImageViewCreateInfo & info,
			VkImageView & value, const VkAllocationCallbacks * const a) noexcept
	{
		return vkCreateImageView(deps.device, &info, a, &value);
	}

	static void destroy(const Deps & deps, const VkImageView & value,
			const VkAllocationCallbacks * const a) noexcept
	{
		vkDestroyImageView(deps.device, value, a);
	}
};

typedef Handle<VkImageView> ImageView;




template <>
struct __HandleTraits<VkRenderPass>
{
	struct Deps {
		VkDevice device;
	};

	static VkResult create(const Deps & deps, const VkRenderPassCreateInfo & info,
			VkRenderPass & value, const VkAllocationCallbacks * const a) noexcept
	{
		return vkCreateRenderPass(deps.device, &info, a, &value);
	}

	static void destroy(const Deps & deps, const VkRenderPass & value,
			const VkAllocationCallbacks * const a) noexcept
	{
		vkDestroyRenderPass(deps.device, value, a);
	}
};

typedef Handle<VkRenderPass> RenderPass;




template <>
struct __HandleTraits<VkFramebuffer>
{
	struct Deps {
		VkDevice device;
	};

	static VkResult create(const Deps & deps, const VkFramebufferCreateInfo & info,
			VkFramebuffer & value, const VkAllocationCallbacks * const a) noexcept
	{
		return vkCreateFramebuffer(deps.device, &info, a, &value);
	}

	static void destroy(const Deps & deps, const VkFramebuffer & value,
			const VkAllocationCallbacks * const a) noexcept
	{
		vkDestroyFramebuffer(deps.device, value, a);
	}
};

typedef Handle<VkFramebuffer> Framebuffer;




template <>
struct __HandleTraits<VkBuffer>
{
	struct Deps {
		VkDevice device;
	};

	static VkResult create(const Deps & deps, const VkBufferCreateInfo & info, VkBuffer & value,
			const VkAllocationCallbacks * const a) noexcept
	{
		return vkCreateBuffer(deps.device, &info, a, &value);
	}

	static void destroy(const Deps & deps, const VkBuffer & value,
			const VkAllocationCallbacks * const a) noexcept
	{
		vkDestroyBuffer(deps.device, value, a);
	}
};

typedef Handle<VkBuffer> Buffer;




template <>
struct __HandleTraits<VkBufferView>
{
	struct Deps {
		VkDevice device;
	};

	static VkResult create(const Deps & deps, const VkBufferViewCreateInfo & info,
			VkBufferView & value, const VkAllocationCallbacks * const a) noexcept
	{
		return vkCreateBufferView(deps.device, &info, a, &value);
	}

	static void destroy(const Deps & deps, const VkBufferView & value,
			const VkAllocationCallbacks * const a) noexcept
	{
		vkDestroyBufferView(deps.device, value, a);
	}
};

typedef Handle<VkBufferView> BufferView;




template <>
struct __HandleTraits<VkSampler>
{
	struct Deps {
		VkDevice device;
	};

	static VkResult create(const Deps & deps, const VkSamplerCreateInfo & info, VkSampler & value,
			const VkAllocationCallbacks * const a) noexcept
	{
		return vkCreateSampler(deps.device, &info, a, &value);
	}

	static void destroy(const Deps & deps, const VkSampler & value,
			const VkAllocationCallbacks * const a) noexcept
	{
		vkDestroySampler(deps.device, value, a);
	}
};

typedef Handle<VkSampler> Sampler;




template <>
struct __HandleTraits<VkDescriptorSetLayout>
{
	struct Deps {
		VkDevice device;
	};

	static VkResult create(const Deps & deps, const VkDescriptorSetLayoutCreateInfo & info,
			VkDescriptorSetLayout & value, const VkAllocationCallbacks * const a) noexcept
	{
		return vkCreateDescriptorSetLayout(deps.device, &info, a, &value);
	}

	static void destroy(const Deps & deps, const VkDescriptorSetLayout & value,
			const VkAllocationCallbacks * const a) noexcept
	{
		vkDestroyDescriptorSetLayout(deps.device, value, a);
	}
};

typedef Handle<VkDescriptorSetLayout> DescriptorSetLayout;




template <>
struct __HandleTraits<VkDescriptorPool>
{
	struct Deps {
		VkDevice device;
	};

	static VkResult create(const Deps & deps, const VkDescriptorPoolCreateInfo & info,
			VkDescriptorPool & value, const VkAllocationCallbacks * const a) noexcept
	{
		return vkCreateDescriptorPool(deps.device, &info, a, &value);
	}

	static void destroy(const Deps & deps, const VkDescriptorPool & value,
			const VkAllocationCallbacks * const a) noexcept
	{
		vkDestroyDescriptorPool(deps.device, value, a);
	}
};

typedef Handle<VkDescriptorPool> DescriptorPool;




template <>
struct __HandleTraits<VkDescriptorSet>
{
	struct Deps {
		VkDevice device;
		VkDescriptorPool descriptorPool;
	};

	static VkResult create(const Deps & deps, const VkDescriptorSetAllocateInfo & info,
			VkDescriptorSet & value, const VkAllocationCallbacks *) noexcept
	{
		return vkAllocateDescriptorSets(deps.device, &info, &value);
	}

	static void destroy(const Deps & deps, const VkDescriptorSet & value,
			const VkAllocationCallbacks *) noexcept
	{
		vkFreeDescriptorSets(deps.device, deps.descriptorPool, 1, &value);
	}
};

typedef Handle<VkDescriptorSet> DescriptorSet;




template <>
struct __HandleTraits<VkPipelineLayout>
{
	struct Deps {
		VkDevice device;
	};

	static VkResult create(const Deps & deps, const VkPipelineLayoutCreateInfo & info,
			VkPipelineLayout & value, const VkAllocationCallbacks * const a) noexcept
	{
		return vkCreatePipelineLayout(deps.device, &info, a, &value);
	}

	static void destroy(const Deps & deps, const VkPipelineLayout & value,
			const VkAllocationCallbacks * const a) noexcept
	{
		vkDestroyPipelineLayout(deps.device, value, a);
	}
};

typedef Handle<VkPipelineLayout> PipelineLayout;




template <>
struct __HandleTraits<VkPipelineCache>
{
	struct Deps {
		VkDevice device;
	};

	static VkResult create(const Deps & deps, const VkPipelineCacheCreateInfo & info,
			VkPipelineCache & value, const VkAllocationCallbacks * const a) noexcept
	{
		return vkCreatePipelineCache(deps.device, &info, a, &value);
	}

	static void destroy(const Deps & deps, const VkPipelineCache & value,
			const VkAllocationCallbacks * const a) noexcept
	{
		vkDestroyPipelineCache(deps.device, value, a);
	}
};

typedef Handle<VkPipelineCache> PipelineCache;




template <>
struct __HandleTraits<VkPipeline>
{
	struct Deps {
		VkDevice device;
		VkPipelineCache cache;
	};

	static VkResult create(const Deps & deps, const VkGraphicsPipelineCreateInfo & info,
			VkPipeline & value, const VkAllocationCallbacks * const a) noexcept
	{
		return vkCreateGraphicsPipelines(deps.device, deps.cache, 1, &info, a, &value);
	}

	static void destroy(const Deps & deps, const VkPipeline & value,
			const VkAllocationCallbacks * const a) noexcept
	{
		vkDestroyPipeline(deps.device, value, a);
	}
};

typedef Handle<VkPipeline> Pipeline;




template <>
struct __HandleTraits<VkShaderModule>
{
	struct Deps {
		VkDevice device;
	};

	static VkResult create(const Deps & deps, const VkShaderModuleCreateInfo & info,
			VkShaderModule & value, const VkAllocationCallbacks * const a) noexcept
	{
		return vkCreateShaderModule(deps.device, &info, a, &value);
	}

	static void destroy(const Deps & deps, const VkShaderModule & value,
			const VkAllocationCallbacks * const a) noexcept
	{
		vkDestroyShaderModule(deps.device, value, a);
	}
};

typedef Handle<VkShaderModule> ShaderModule;




struct MemoryMapper
{
	MemoryMapper() = default;

	MemoryMapper(const VkDevice device, const VkDeviceMemory memory, const VkDeviceSize offset,
			const VkDeviceSize size) :
		device(device),
		memory(memory)
	{
		checkResult(vkMapMemory(device, memory, offset, size, 0,
				reinterpret_cast<void**>(&data)));
	}

	MemoryMapper(const DeviceMemory & memory, const VkDeviceSize offset,
			const VkDeviceSize size) :
		device(memory.device),
		memory(memory)
	{
		checkResult(vkMapMemory(device, memory, offset, size, 0,
				reinterpret_cast<void**>(&data)));
	}

	MemoryMapper(MemoryMapper && o) noexcept :
		device(o.device),
		memory(o.memory),
		data(o.data)
	{
		o.device = nullptr;
		o.memory = nullptr;
		o.data = nullptr;
	}

	MemoryMapper & operator=(MemoryMapper && o) noexcept
	{
		std::swap(device, o.device);
		std::swap(memory, o.memory);
		std::swap(data, o.data);
		return *this;
	}

	~MemoryMapper() noexcept
	{
		if (memory) {
			vkUnmapMemory(device, memory);
		}
	}

	VkDevice device = nullptr;
	VkDeviceMemory memory = nullptr;
	uint8_t * data = nullptr;
};




template <>
struct __HandleTraits<VkSurfaceKHR>
{
	struct Deps {
		VkInstance instance;
	};

	static void destroy(const Deps & deps, const VkSurfaceKHR & value,
			const VkAllocationCallbacks * const a) noexcept
	{
		vkDestroySurfaceKHR(deps.instance, value, a);
	}
};

typedef Handle<VkSurfaceKHR> Surface;




template <>
struct __HandleTraits<VkSwapchainKHR>
{
	struct Deps {
		VkDevice device;
	};

	static VkResult create(const Deps & deps, const VkSwapchainCreateInfoKHR & info,
			VkSwapchainKHR & value, const VkAllocationCallbacks * const a) noexcept
	{
		return vkCreateSwapchainKHR(deps.device, &info, a, &value);
	}

	static void destroy(const Deps & deps, const VkSwapchainKHR & value,
			const VkAllocationCallbacks * const a) noexcept
	{
		vkDestroySwapchainKHR(deps.device, value, a);
	}
};

typedef Handle<VkSwapchainKHR> Swapchain;




namespace Tools {

extern FLAT_VULKAN_EXPORT void submitAndWait(VkCommandBuffer commandBuffer, VkQueue queue);

extern FLAT_VULKAN_EXPORT uint32_t getMemoryTypeIndex(
		const VkPhysicalDeviceMemoryProperties & gpuMemoryProperties,
		uint32_t typeBits, VkMemoryPropertyFlags p, VkBool32 * found = nullptr);

}




}} // Flat::Vulkan
