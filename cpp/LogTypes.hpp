
#include <Flat/Debug/Log.hpp>

#include <vulkan/vulkan.h>

#include "Vulkan.hpp"




namespace Flat { namespace Vulkan {

FLAT_VULKAN_EXPORT const char * resultName(const VkResult result) noexcept;

}}




namespace Flat { namespace Debug {

template<>
FLAT_VULKAN_EXPORT void Log::append<VkResult>(const VkResult & v) noexcept;

template <>
FLAT_VULKAN_EXPORT void Log::append<VkPhysicalDeviceType>(const VkPhysicalDeviceType & v) noexcept;

template <typename T>
inline void operator<<(Log::Append & append, const Vulkan::Handle<T> & h) noexcept
{ append.log().appendPointer(static_cast<T>(h)); }

}}
